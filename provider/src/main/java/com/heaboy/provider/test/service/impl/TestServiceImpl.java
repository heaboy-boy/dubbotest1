package com.heaboy.provider.test.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heaboy.service.test.entity.Test;
import com.heaboy.provider.test.mapper.TestMapper;
import com.heaboy.service.test.service.ITestService;
import org.springframework.util.ObjectUtils;
import org.apache.dubbo.config.annotation.Service;

/**
 * <p>
  * $!{table.comment} 服务类
  * </p>
 *
 * @author heaboy
 * @since 2020-02-17
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements ITestService {

    @Override
    public IPage<Test> index(Page<Test> page ,Test test){

        QueryWrapper<Test> queryWrapper = new QueryWrapper<Test>();
        if(!ObjectUtils.isEmpty(test.getTestName())) {
            queryWrapper = queryWrapper.like("testName",test.getTestName());
        }
        if(!ObjectUtils.isEmpty(test.getTestSex())) {
            queryWrapper = queryWrapper.like("testSex",test.getTestSex());
        }
        IPage<Test> pageInfo = page(page,queryWrapper);
        return pageInfo;
    }
}
