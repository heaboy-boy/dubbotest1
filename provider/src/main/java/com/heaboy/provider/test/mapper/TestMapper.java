package com.heaboy.provider.test.mapper;

import com.heaboy.service.test.entity.Test;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author heaboy
 * @since 2020-02-17
 */
public interface TestMapper extends BaseMapper<Test> {

}
